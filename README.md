CSPP_MQTT extends the CSPP\_Core package of the CSPP\_-Project. 

Refer to https://git.gsi.de/EE-LV/CSPP/CSPP/-/wikis/home for CS++ project overview, details and documentation.

LabVIEW 2021 SP1 is the currently used development environment.

Related documents and information
=================================

- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/CSPP_MQTT.git
- Documentation:
  - Refer to package folder
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview
  - Stephen Cope: "MQTT For Complete Beginners: Learn The Basics of the MQTT Protocol (English Edition)"
  - Stephen Cope: "Working with the Paho Python MQTT Client (English Edition)"

GIT Submodules
==============
This package can be used as submodule
- Packages\CSPP_MQTT:
  - CSPP_MQTTBroker.lvlib
  - CSPP_MQTTConnection.lvlib
  - CSPP_MQTTMonitor.lvlib
  - CSPP_MQTTSVInterface.lvlib
  - CSPP_MQTTSVInterface_SV.lvlib

External Dependencies
---------------------

- CSPP_Core: http://github.com/HB-GSI/CSPP_Core
- CSPP_MQTTSVInterface depends on LabVIEW Data Logging and Supervisory Control Module
- Some VIPM Packages dealing with MQTT from LabVIEW Open Source Project. Refer to VIPM-MQTT.PNG for details.
  - You can find the sources at https://github.com/LabVIEW-Open-Source
  - Refer also to following youtube videos for an overview of this liberaies.
    - https://www.youtube.com/watch?v=Y-jrwyfD9DU&feature=youtu.be
    - https://youtu.be/Y-jrwyfD9DU

Getting started:
=================================
- Add CSPP_MQTTContent.vi into your own LabVIEW project and
- Add it to your project specific CSPP_UserContent.vi
- Move lvlib's from dependencies to a virtual folder in your project.
- You need to extend your project specific ini-file.
  - A sample ini-file, CSPP_MQTT.ini, should be available on disk in the corresponding package folder.
- You need to launch a MQTT request broker somewhere.
  - Mosquitto is an open source MQTT broker, https://mosquitto.org
		○ A test broker is available, https://test.mosquitto.org

Known issues and limitations:
=================================
- Each CSPP_MQTTConnection and will create its own unique client connection. This is maybe problematic if number of PVConnection is high.
- Each CSPP_MQTTMonitor, CSPP_MQTTSVInterface will create its own unique client connection. This is maybe tolerable.
- CSPP_MQTTSVInterface:Monitor MQTT.vi handles tag datatype "custom" as error cluster.  "u32BitField", "variant", "fixed point" and other as variant which may cause error.
- Refer also to issue tracker at https://github.com/LabVIEW-Open-Source
  - Broker: https://github.com/LabVIEW-Open-Source/LV-MQTT-Broker/issues
  - Client: https://github.com/LabVIEW-Open-Source/MQTT-Client/issues
  - Messages with Qos>0 are handled synchronously and can cause performance issues.
    - https://github.com/LabVIEW-Open-Source/LV-MQTT-Broker/issues/165
    
Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2022  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.